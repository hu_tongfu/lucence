package com.util;

import java.util.ArrayList;
import java.util.List;

/**
 * LucenceConvert 分页对象
 *
 * @author hutongfu
 * @since 2018-01-08 11:29
 */
public class LucencePage<T> {

    /**
     * 当前页码
     */
    private int pageNo = 1;

    /**
     * 每页大小
     */
    private int pageSize = 10;

    /**
     * 数据
     */
    private List<T> data = new ArrayList<T>();

    /**
     * 总记录数
     */
    private int total;

    /**
     * 首页索引
     */
    private int first;

    /**
     * 尾页索引
     */
    private int last;

    /**
     * 上一页索引
     */
    private int prev;

    /**
     * 下一页索引
     */
    private int next;

    /**
     * 是否是第一页
     */
    private boolean firstPage;

    /**
     * 是否是最后一页
     */
    private boolean lastPage;

    public LucencePage() {
    }

    public LucencePage(int pageNo, int pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    /***
     * 起始索引
     * @return
     */
    public int getFirstResult() {
        return (pageNo - 1) * pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
        this.initialize();
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    public int getPrev() {
        return prev;
    }

    public void setPrev(int prev) {
        this.prev = prev;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public boolean isFirstPage() {
        return firstPage;
    }

    public void setFirstPage(boolean firstPage) {
        this.firstPage = firstPage;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * 初始化参数
     */
    public void initialize() {
        //1
        this.first = 1;
        this.last = (int) (total / (this.pageSize < 1 ? 20 : this.pageSize) + first - 1);

        if (this.total % this.pageSize != 0 || this.last == 0) {
            this.last++;
        }
        if (this.last < this.first) {
            this.last = this.first;
        }

        if (this.pageNo <= 1) {
            this.pageNo = this.first;
            this.firstPage = true;
        }

        if (this.pageNo >= this.last) {
            this.pageNo = this.last;
            this.lastPage = true;
        }

        if (this.pageNo < this.last - 1) {
            this.next = this.pageNo + 1;
        } else {
            this.next = this.last;
        }

        if (this.pageNo > 1) {
            this.prev = this.pageNo - 1;
        } else {
            this.prev = this.first;
        }

        //2
        // 如果当前页小于首页
        if (this.pageNo < this.first) {

            this.pageNo = this.first;
        }
        // 如果当前页大于尾页
        if (this.pageNo > this.last) {
            this.pageNo = this.last;
        }

    }

    @Override
    public String toString() {
        return "LucencePage{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", data=" + data +
                ", total=" + total +
                '}';
    }
}
