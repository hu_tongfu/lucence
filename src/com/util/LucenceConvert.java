package com.util;

import org.apache.lucene.document.Document;

/**
 * lucence Document 对象转换成自定义对象
 *
 * @author hutongfu
 * @since 2018-01-08 13:23
 */
public interface LucenceConvert<T> {

    public T documentToObj(Document document);

    public Document objToDocument(T t);
}
