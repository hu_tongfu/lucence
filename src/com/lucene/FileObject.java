package com.lucene;

/**
 * 测试对象
 *
 * @author hutongfu
 * @since 2018-01-08 15:06
 */
public class FileObject {

    private String fileName;

    private String fileContext;

    private long fileSize;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileContext() {
        return fileContext;
    }

    public void setFileContext(String fileContext) {
        this.fileContext = fileContext;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String toString() {
        return "FileObject{" +
                "fileName='" + fileName + '\'' +
                ", fileContext='" + fileContext + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
