# lucence
lucence 全文搜索引擎工具
1. 搜索方式
- TermQuery:
     根据词进行搜索（只能从文本中进行搜索）
- QueryParser：
     根据域名进行搜索，可以设置默认搜索域。推荐使用（只能从文本中进行搜索）
- BooleanQuery：
     组合查询，可以设置组合条件，not，or，and。从多个域进行查询。
     MUST相当于and,SHOULD相当于or,NUST_NOT相当于not
- MathcAllDocsQuery：
     查询出所有文档
- MultiFieldQuery:
     可以从多个域中进行查询，只要这些域中有关键词的存在就查询出来
- NumericRangeQuery:
     从数值范围进行搜索

2. 创建数据库数据索引
- 自定义一个对象转换器，将数据库中的数据对象，转换成 Document 对象
